# ArchPackages
This is repository for the ones, who use the best distro ever - ArchLinux - and
wants to install following packages using package manager.

Available packages:
* cheby-git
* hdlmake-git
* pyloggingformatter - just at small module to color the python logging
* vunit-git

## INSTALL 
1. clone the repository

```
$> git clone ssh://git@gitlab.cern.ch:7999/utils/archpackages.git
```

2. go to the folder of the package you want to install

```
$> cd archpackages/cheby-git
```

3. launch installation
```
$> makepkg -si
```

4. enjoy

## NOTES

